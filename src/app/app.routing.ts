import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    //  data: {
    //    title: 'Home'
    //  },
    children: [
      {
        path: '',
       // component: DefaultLayoutComponent,
        data: {
          title: 'Implementacion'
        },
        children :[
          {
            path: 'Implementacion/CondicionesPrevias',
            loadChildren: () => import('./views/Implementacion/condiciones-previas/condiciones-previas.module').then(m => m.CondicionesPreviasModule)
          },
          {
            path: 'Implementacion/Planificacion',
            loadChildren: () => import('./views/Implementacion/Planificacion/planificacion.module').then(m => m.PlanificacionModule)
          },
          {
            path: 'Implementacion/Monitoreo',
            loadChildren: () => import('./views/Implementacion/monitoreo/monitoreo.module').then(m => m.MonitoreoModule)
          },

          {
            path: 'Implementacion/Ejecucion',
            loadChildren: () => import('./views/Implementacion/Ejecucion/ejecucion.module').then(m => m.EjecucionModule)
          },


        ]

      },
      {
        path: 'AreaUsuario',
        loadChildren: () => import('./views/area-usuario/area-usuario.module').then(m => m.AreaUsuarioModule)
      },
      {
        path: 'Reportes',
        loadChildren: () => import('./views/Reportes/reportes.module').then(m => m.ReportesModule)
      },
      {
        path: 'Entidades',
        loadChildren: () => import('./views/sectorista/sectorista.module').then(m => m.SectoristaModule)
      },
     





      {
        path: 'base',
        loadChildren: () => import('./views/base/base.module').then(m => m.BaseModule)
      },
      {
        path: 'buttons',
        loadChildren: () => import('./views/buttons/buttons.module').then(m => m.ButtonsModule)
      },
      {
        path: 'charts',
        loadChildren: () => import('./views/chartjs/chartjs.module').then(m => m.ChartJSModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'icons',
        loadChildren: () => import('./views/icons/icons.module').then(m => m.IconsModule)
      },
      {
        path: 'notifications',
        loadChildren: () => import('./views/notifications/notifications.module').then(m => m.NotificationsModule)
      },
      {
        path: 'theme',
        loadChildren: () => import('./views/theme/theme.module').then(m => m.ThemeModule)
      },
      {
        path: 'widgets',
        loadChildren: () => import('./views/widgets/widgets.module').then(m => m.WidgetsModule)
      }
    ]
  },


  /** */

  // {
  //   path: '',
  //   component: DefaultLayoutComponent,
  //   data: {
  //     title: 'Etapas'
  //   },
  //   children: [
      
  //     {
  //       path: 'etapa1',
  //       loadChildren: () => import('./views/etapas/etapa-1/etapa-1.module').then(m => m.Etapa1Module)
  //     }

  //   ]
  // },

  /** */
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
