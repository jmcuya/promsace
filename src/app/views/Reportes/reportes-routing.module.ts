import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GenerarReportesComponent } from './generar-reportes/generar-reportes.component';

const routes: Routes = 
[
  {
    path : '',
    data : {
      title : 'Reportes' //ruta principal despues del home, breadcrumb
    },
    children : [
        {
          path: '',
          redirectTo: 'GenerarReporte' // ruta de breadcrumb principal
        },
        {
          path: 'GenerarReporte', // ruta del componente en URL 
          component: GenerarReportesComponent ,
          data: {
            title: 'Generar' // nombre de breadrumb
          }
        }
     
    ]
    
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportesRoutingModule { }
