import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportesRoutingModule } from './reportes-routing.module';
import { GenerarReportesComponent } from './generar-reportes/generar-reportes.component';


@NgModule({
  declarations: [
    GenerarReportesComponent
  ],
  imports: [
    CommonModule,
    ReportesRoutingModule
  ]
})
export class ReportesModule { }
