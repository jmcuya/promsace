import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.scss']
})
export class RegistrarComponent implements OnInit {
  mostrarNombre   : boolean = false;
  nombreReniec       : string = "José Manuel Cuya Yaguana";

  constructor() { }

  ngOnInit(): void {
  }
  validarReniecClic(){
    this.mostrarNombre = true;  
  }
  restablecerReniec(){
    this.mostrarNombre = false;
  }
  confirmarGuardar(){
    Swal.fire({
      title: '¿Está seguro de grabar el registro?',
      text: "Podrá modificarlo luego",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#0a5d4c',
      cancelButtonColor: '#a2187e',
      confirmButtonText: 'Guardar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Guardado',
          'El registro ha sido guardado',
          'success'
        )
      }
    });
   }
}
