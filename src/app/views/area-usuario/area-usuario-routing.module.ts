import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GestionarComponent } from './gestionar/gestionar.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { CuentaComponent } from './cuenta/cuenta.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { CambioClaveComponent } from './cambio-clave/cambio-clave.component';

const routes: Routes = 
[
  {
    path : '',
    data : {
      title : 'Area de Usuarios' 
    },
    children : [
        {
          path: '',
          redirectTo: 'GestionUsuario' 
        },
        {
          path: 'GestionUsuario', 
          component: GestionarComponent ,
          data: {
            title: 'Gestionar' 
          }
        },
        {
          path: 'RegistroUsuario',
          component: RegistrarComponent,
          data: {
            title: 'Registrar '
          }
        },
        {
          path: 'MiCuenta',
          component: CuentaComponent,
          data: {
            title: 'Mi Cuenta '
          }
        },     
        {
          path: 'Config',
          component: ConfiguracionComponent,
          data: {
            title: 'Configuración'
          }
        },
        {
          path: 'CambioClave',
          component: CambioClaveComponent,
          data: {
            title: 'Cambiar Clave'
          }
        },
    ]
    
    }
    
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AreaUsuarioRoutingModule { }
