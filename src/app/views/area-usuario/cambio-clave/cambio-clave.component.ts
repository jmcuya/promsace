import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cambio-clave',
  templateUrl: './cambio-clave.component.html',
  styleUrls: ['./cambio-clave.component.scss']
})
export class CambioClaveComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  confirmarGuardar(){
    Swal.fire({
      title: '¿Está seguro de grabar el registro?',
      text: "Podrá modificarlo luego",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#0a5d4c',
      cancelButtonColor: '#a2187e',
      confirmButtonText: 'Guardar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Guardado',
          'El registro ha sido guardado',
          'success'
        )
      }
    });
   }
}
