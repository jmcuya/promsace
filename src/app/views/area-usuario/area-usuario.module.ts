import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AreaUsuarioRoutingModule } from './area-usuario-routing.module';
import { GestionarComponent } from './gestionar/gestionar.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { CuentaComponent } from './cuenta/cuenta.component';
import { ConfiguracionComponent } from './configuracion/configuracion.component';
import { CambioClaveComponent } from './cambio-clave/cambio-clave.component';


@NgModule({
  declarations: [
    GestionarComponent,
    RegistrarComponent,
    CuentaComponent,
    ConfiguracionComponent,
    CambioClaveComponent
  ],
  imports: [
    CommonModule,
    AreaUsuarioRoutingModule
  ]
})
export class AreaUsuarioModule { }
