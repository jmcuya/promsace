import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SectoristaRoutingModule } from './sectorista-routing.module';
import { GestionarEntidadComponent } from './gestionar-entidad/gestionar-entidad.component';
import { ModificarEntidadComponent } from './modificar-entidad/modificar-entidad.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';


@NgModule({
  declarations: [
    GestionarEntidadComponent,
    ModificarEntidadComponent
  ],
  imports: [
    CommonModule,
    CKEditorModule,
    SectoristaRoutingModule
  ]
})
export class SectoristaModule { }
