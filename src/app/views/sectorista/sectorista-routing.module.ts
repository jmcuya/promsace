import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GestionarEntidadComponent } from './gestionar-entidad/gestionar-entidad.component';
import { ModificarEntidadComponent } from './modificar-entidad/modificar-entidad.component';


const routes: Routes = 
[
  {
    path : '',
    data : {
      title : 'Entidades' 
    },
    children : [
        {
          path: '',
          redirectTo: 'login' 
        },
        {
          path: 'GestionarEntidad', 
          component: GestionarEntidadComponent ,
          data: {
            title: 'Gestionar Entidad' 
          }
        },
        {
          path: 'ModificarEntidad',
          component: ModificarEntidadComponent,
          data: {
            title: 'Modificar Entidad'
          }
        },
    ]
    
    }
    
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SectoristaRoutingModule { }
