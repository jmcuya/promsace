import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarEntidadComponent } from './modificar-entidad.component';

describe('ModificarEntidadComponent', () => {
  let component: ModificarEntidadComponent;
  let fixture: ComponentFixture<ModificarEntidadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModificarEntidadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarEntidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
