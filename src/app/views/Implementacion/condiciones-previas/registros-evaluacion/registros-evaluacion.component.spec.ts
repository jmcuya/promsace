import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrosEvaluacionComponent } from './registros-evaluacion.component';

describe('RegistrosEvaluacionComponent', () => {
  let component: RegistrosEvaluacionComponent;
  let fixture: ComponentFixture<RegistrosEvaluacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrosEvaluacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrosEvaluacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
