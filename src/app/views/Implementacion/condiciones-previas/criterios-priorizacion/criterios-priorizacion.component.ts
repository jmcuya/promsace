import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-criterios-priorizacion',
  templateUrl: './criterios-priorizacion.component.html',
  styleUrls: ['./criterios-priorizacion.component.scss']
})
export class CriteriosPriorizacionComponent implements OnInit {
  addForm: FormGroup;
  rowsComponent1: FormArray = this.fb.array([]);
  itemForm: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
    this.rowsComponent1.push(this.createItemFormGroup());
  }

  initForm(){
    this.addForm = this.fb.group({
    rows1 : this.rowsComponent1
   });
  }

  onAddRowComponent1() {
    this.rowsComponent1.push(this.createItemFormGroup());
  }

  onRemoveRowComponent1(rowIndex:number){
    this.rowsComponent1.removeAt(rowIndex);
  }

  createItemFormGroup(): FormGroup {
    return this.fb.group({
      fm_Criterio: null,
      fm_Peso: null,
    });
  }

}
