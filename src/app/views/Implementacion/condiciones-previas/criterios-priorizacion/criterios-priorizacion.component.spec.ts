import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriteriosPriorizacionComponent } from './criterios-priorizacion.component';

describe('CriteriosPriorizacionComponent', () => {
  let component: CriteriosPriorizacionComponent;
  let fixture: ComponentFixture<CriteriosPriorizacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriteriosPriorizacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriteriosPriorizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
