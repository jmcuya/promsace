import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlcanceServicioComponent } from './alcance-servicio.component';

describe('AlcanceServicioComponent', () => {
  let component: AlcanceServicioComponent;
  let fixture: ComponentFixture<AlcanceServicioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlcanceServicioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlcanceServicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
