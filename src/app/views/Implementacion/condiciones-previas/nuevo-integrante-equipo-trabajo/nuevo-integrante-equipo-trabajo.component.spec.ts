import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoIntegranteEquipoTrabajoComponent } from './nuevo-integrante-equipo-trabajo.component';

describe('NuevoIntegranteEquipoTrabajoComponent', () => {
  let component: NuevoIntegranteEquipoTrabajoComponent;
  let fixture: ComponentFixture<NuevoIntegranteEquipoTrabajoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevoIntegranteEquipoTrabajoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoIntegranteEquipoTrabajoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
