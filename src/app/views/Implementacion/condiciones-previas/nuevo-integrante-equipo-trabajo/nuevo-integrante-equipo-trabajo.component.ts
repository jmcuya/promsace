import { Component, OnInit, ViewChild } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-nuevo-integrante-equipo-trabajo',
  templateUrl: './nuevo-integrante-equipo-trabajo.component.html',
  styleUrls: ['./nuevo-integrante-equipo-trabajo.component.scss']
})
export class NuevoIntegranteEquipoTrabajoComponent implements OnInit {

  mostrarNombre   : boolean = false;
  nombreReniec    : string = "José Manuel Cuya Yaguana";

  constructor() { }

  ngOnInit(): void {
  }
  validarReniecClic(){
    this.mostrarNombre = true;  
  }
  restablecerReniec(){
    this.mostrarNombre = false;
  }

  confirmarGuardar(){
    Swal.fire({
      title: '¿Está seguro de grabar el registro?',
      text: "Podrá modificarlo luego",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#0a5d4c',
      cancelButtonColor: '#a2187e',
      confirmButtonText: 'Guardar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Guardado',
          'El registro ha sido guardado',
          'success'
        )
      }
    });
   }
  
   confirmarFinalizar(){
    Swal.fire({
      title: '¿Está seguro de finalizar el registro?',
      text: "Ya no podrá modificarlo luego",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#0a5d4c',
      cancelButtonColor: '#a2187e',
      confirmButtonText: 'Finalizar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Finalizado',
          'El registro ha sido finalizado',
          'success'
        )
      }
    });
   }
  
   
}
