import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EvaluarEntidadComponent } from './evaluar-entidad/evaluar-entidad.component';
import { DesignarMaximaAutoridadComponent} from './designar-maxima-autoridad/designar-maxima-autoridad.component';
import { DesignarResponsableComponent } from './designar-responsable/designar-responsable.component';
import { EquiposTrabajoComponent } from './equipos-trabajo/equipos-trabajo.component';
import { NuevoIntegranteEquipoTrabajoComponent } from './nuevo-integrante-equipo-trabajo/nuevo-integrante-equipo-trabajo.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { AlcanceServicioComponent } from './alcance-servicio/alcance-servicio.component';
import { CadenaValorServicioComponent } from './cadena-valor-servicio/cadena-valor-servicio.component';
import { NuevoServicioComponent } from './nuevo-servicio/nuevo-servicio.component';
import { CriteriosPriorizacionComponent } from './criterios-priorizacion/criterios-priorizacion.component';
import { CriteriosDePriorizacionComponent } from './criterios-de-priorizacion/criterios-de-priorizacion.component';
import { InformeComponent } from './informe/informe.component';
import { RegistrosEvaluacionComponent } from './registros-evaluacion/registros-evaluacion.component';

const routes: Routes = 
[
  {
    path : '',
    data : {
      title : 'Condiciones Previas' //ruta principal despues del home, breadcrumb
    },
    children : [
        {
          path: '',
          redirectTo: 'EvaluaEntidad' // ruta de breadcrumb principal
        },
        {
          path: 'RegistrosEvaluaEntidad', // ruta del componente en URL 
          component: RegistrosEvaluacionComponent ,
          data: {
            title: 'Registros de evaluaciones' // nombre de breadrumb
          }
        },
        {
          path: 'EvaluaEntidad', // ruta del componente en URL 
          component: EvaluarEntidadComponent ,
          data: {
            title: 'Evaluar Entidad' // nombre de breadrumb
          }
        },
        {
          path: 'DesignarMaximaAutoridad',
          component: DesignarMaximaAutoridadComponent,
          data: {
            title: 'Designar Maxima Autoridad'
          }
        },
        {
          path: 'DesignarResponsable',
          component: DesignarResponsableComponent,
          data: {
            title: 'Designar Responsable'
          }
        },
        {
          path: 'EquipoTrabajo',
          component: EquiposTrabajoComponent,
          data: {
            title: 'Equipo de trabajo'
          }
        },
        {
          path: 'NuevoIntegranteEquipoTrabajo',
          component: NuevoIntegranteEquipoTrabajoComponent,
          data: {
            title: 'Nuevo Integrante de Equipo de trabajo'
          }
        },
        {
          path: 'Servicios',
          component: ServiciosComponent,
          data: {
            title: 'Servicios'
          }
        },
        {
          path: 'AlcanceServicio',
          component: AlcanceServicioComponent,
          data: {
            title: 'Alcance de Servicio'
          }
        },
        {
          path: 'CadenaValorServicio',
          component: CadenaValorServicioComponent,
          data: {
            title: 'Cadena de Valor de Servicios'
          }
        },
        {
          path: 'NuevoServicioPriorizacion',
          component: NuevoServicioComponent,
          data: {
            title: 'Nuevos Servicio - Priorización'
          }
        },
        {
          path: 'CritPriorizacion',
          component: CriteriosPriorizacionComponent,
          data: {
            title: 'Criterio de priorización'
          }
        },
        {
          path: 'CritDePriorizacion',
          component: CriteriosDePriorizacionComponent,
          data: {
            title: 'Criterio de priorización'
          }
        },
        {
          path: 'Informe',
          component: InformeComponent,
          data: {
            title: 'Informe'
          }
        }
     
    ]
    
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CondicionesPreviasRoutingModule { }
