import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriteriosDePriorizacionComponent } from './criterios-de-priorizacion.component';

describe('CriteriosDePriorizacionComponent', () => {
  let component: CriteriosDePriorizacionComponent;
  let fixture: ComponentFixture<CriteriosDePriorizacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriteriosDePriorizacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriteriosDePriorizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
