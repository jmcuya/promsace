import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignarResponsableComponent } from './designar-responsable.component';

describe('DesignarResponsableComponent', () => {
  let component: DesignarResponsableComponent;
  let fixture: ComponentFixture<DesignarResponsableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DesignarResponsableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignarResponsableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
