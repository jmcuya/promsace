import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CondicionesPreviasRoutingModule } from './condiciones-previas-routing.module';
import { EvaluarEntidadComponent } from './evaluar-entidad/evaluar-entidad.component';
import { DesignarResponsableComponent } from './designar-responsable/designar-responsable.component';
import { EquiposTrabajoComponent } from './equipos-trabajo/equipos-trabajo.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { CriteriosPriorizacionComponent } from './criterios-priorizacion/criterios-priorizacion.component';
import { InformeComponent } from './informe/informe.component';
import { DesignarMaximaAutoridadComponent } from './designar-maxima-autoridad/designar-maxima-autoridad.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../../../app.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
//import { PdfViewerModule } from 'ng2-pdf-viewer';
// import the PdfViewer Module for the PDF Viewer component
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { PdfViewerModule} from '@syncfusion/ej2-angular-pdfviewer';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { ModalModule } from 'ngx-bootstrap/modal';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CriteriosDePriorizacionComponent } from './criterios-de-priorizacion/criterios-de-priorizacion.component';
import { NuevoIntegranteEquipoTrabajoComponent } from './nuevo-integrante-equipo-trabajo/nuevo-integrante-equipo-trabajo.component';
import { NuevoServicioComponent } from './nuevo-servicio/nuevo-servicio.component';
import { AlcanceServicioComponent } from './alcance-servicio/alcance-servicio.component';
import { CadenaValorServicioComponent } from './cadena-valor-servicio/cadena-valor-servicio.component';
import { RegistrosEvaluacionComponent } from './registros-evaluacion/registros-evaluacion.component';


@NgModule({
  declarations: [
    EvaluarEntidadComponent,
    DesignarResponsableComponent,
    EquiposTrabajoComponent,
    ServiciosComponent,
    CriteriosPriorizacionComponent,
    InformeComponent,
    DesignarMaximaAutoridadComponent,
    CriteriosDePriorizacionComponent,
    NuevoIntegranteEquipoTrabajoComponent,
    NuevoServicioComponent,
    AlcanceServicioComponent,
    CadenaValorServicioComponent,
    RegistrosEvaluacionComponent,
  ],
  imports: [
    CommonModule,
    CondicionesPreviasRoutingModule,
    CKEditorModule,
    FormsModule,
    //BrowserModule,
    //AppRoutingModule,
    NgbModule,
    //FormsModule,
    NgxExtendedPdfViewerModule,
    ReactiveFormsModule,
    PdfViewerModule,
    ModalModule.forRoot(),


    TabsModule,
    ChartsModule,
    AccordionModule,
    FormsModule,
    ReactiveFormsModule,
    CollapseModule.forRoot(),

    CondicionesPreviasRoutingModule
  ]
})
export class CondicionesPreviasModule { }