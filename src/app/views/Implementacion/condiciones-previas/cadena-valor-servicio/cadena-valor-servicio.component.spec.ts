import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadenaValorServicioComponent } from './cadena-valor-servicio.component';

describe('CadenaValorServicioComponent', () => {
  let component: CadenaValorServicioComponent;
  let fixture: ComponentFixture<CadenaValorServicioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadenaValorServicioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadenaValorServicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
