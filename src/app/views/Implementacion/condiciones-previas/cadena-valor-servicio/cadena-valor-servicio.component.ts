import { Component, OnInit, ViewChild } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-cadena-valor-servicio',
  templateUrl: './cadena-valor-servicio.component.html',
  styleUrls: ['./cadena-valor-servicio.component.scss']
})
export class CadenaValorServicioComponent implements OnInit {

  Editor = ClassicEditor;
  model1 =  { 
      editorData :  '<p> ¡Hola, mundo! </p>' 
  } ; 
  model2 =  { 
    editorData :  '<p> ¡Hola, mundo! </p>' 
  } ; 
  model3 =  { 
    editorData :  '<p> ¡Hola, mundo! </p>' 
  } ; 
  model4 =  { 
    editorData :  '<p> ¡Hola, mundo! </p>' 
  } ; 
  model5 =  { 
    editorData :  '<p> ¡Hola, mundo! </p>' 
  } ; 
  constructor() { }

  ngOnInit(): void {
  }

  public onReady( editor ) {
    editor.ui.getEditableElement().parentElement.insertBefore(
        editor.ui.view.toolbar.element,
        editor.ui.getEditableElement()
    );
    console.log("editor" ,editor)
}

}
