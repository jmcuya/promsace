import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-designar-maxima-autoridad',
  templateUrl: './designar-maxima-autoridad.component.html',
  styleUrls: ['./designar-maxima-autoridad.component.scss']
})
export class DesignarMaximaAutoridadComponent implements OnInit {
  mostrarNombre   : boolean = false;
  nombreReniec       : string = "José Manuel Cuya Yaguana";

  constructor() { }

  ngOnInit(): void {
  }
  validarReniecClic(){
    this.mostrarNombre = true;  
  }
  restablecerReniec(){
    this.mostrarNombre = false;
  }
  
  confirmarGuardar(){
   Swal.fire({
     title: '¿Está seguro de grabar el registro?',
     text: "Podrá modificarlo luego",
     icon: 'warning',
     showCancelButton: true,
     cancelButtonText: 'Cancelar',
     confirmButtonColor: '#0a5d4c',
     cancelButtonColor: '#a2187e',
     confirmButtonText: 'Guardar'
   }).then((result) => {
     if (result.isConfirmed) {
       Swal.fire(
         'Guardado',
         'El registro ha sido guardado',
         'success'
       )
     }
   });
  }
 
  confirmarFinalizar(){
   Swal.fire({
     title: '¿Está seguro de finalizar el registro?',
     text: "Ya no podrá modificarlo luego",
     icon: 'warning',
     showCancelButton: true,
     cancelButtonText: 'Cancelar',
     confirmButtonColor: '#0a5d4c',
     cancelButtonColor: '#a2187e',
     confirmButtonText: 'Finalizar'
   }).then((result) => {
     if (result.isConfirmed) {
       Swal.fire(
         'Finalizado',
         'El registro ha sido finalizado',
         'success'
       )
     }
   });
  }
 
}
