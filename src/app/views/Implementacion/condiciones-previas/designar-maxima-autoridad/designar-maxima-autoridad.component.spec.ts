import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignarMaximaAutoridadComponent } from './designar-maxima-autoridad.component';

describe('DesignarMaximaAutoridadComponent', () => {
  let component: DesignarMaximaAutoridadComponent;
  let fixture: ComponentFixture<DesignarMaximaAutoridadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DesignarMaximaAutoridadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignarMaximaAutoridadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
