import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluarEntidadComponent } from './evaluar-entidad.component';

describe('EvaluarEntidadComponent', () => {
  let component: EvaluarEntidadComponent;
  let fixture: ComponentFixture<EvaluarEntidadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EvaluarEntidadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluarEntidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
