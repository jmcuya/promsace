import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquiposTrabajoComponent } from './equipos-trabajo.component';

describe('EquiposTrabajoComponent', () => {
  let component: EquiposTrabajoComponent;
  let fixture: ComponentFixture<EquiposTrabajoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquiposTrabajoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquiposTrabajoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
