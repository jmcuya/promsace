import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalizarAutodiagnosticoComponent } from './analizar-autodiagnostico.component';

describe('AnalizarAutodiagnosticoComponent', () => {
  let component: AnalizarAutodiagnosticoComponent;
  let fixture: ComponentFixture<AnalizarAutodiagnosticoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnalizarAutodiagnosticoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalizarAutodiagnosticoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
