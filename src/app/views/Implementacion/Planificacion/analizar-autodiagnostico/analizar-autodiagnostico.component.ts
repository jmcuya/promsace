import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-analizar-autodiagnostico',
  templateUrl: './analizar-autodiagnostico.component.html',
  styleUrls: ['./analizar-autodiagnostico.component.scss']
})
export class AnalizarAutodiagnosticoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  

    // lineChart
    public lineChartData: Array<any> = [
      {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
      {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
      {data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C'}
    ];
    public lineChartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
    public lineChartOptions: any = {
      animation: false,
      responsive: true
    };
    public lineChartColours: Array<any> = [
      { // grey
        backgroundColor: 'rgba(148,159,177,0.2)',
        borderColor: 'rgba(148,159,177,1)',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
      },
      { // dark grey
        backgroundColor: 'rgba(77,83,96,0.2)',
        borderColor: 'rgba(77,83,96,1)',
        pointBackgroundColor: 'rgba(77,83,96,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(77,83,96,1)'
      },
      { // grey
        backgroundColor: 'rgba(148,159,177,0.2)',
        borderColor: 'rgba(148,159,177,1)',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
      }
    ];
    public lineChartLegend = true;
    public lineChartType = 'line';
  
    // barChart
    public barChartOptions: any = {
      scaleShowVerticalLines: false,
      responsive: true
    };
    public barChartLabels: string[] = [
      'CONOCER LAS NECESIDADES Y EXPECTATIVAS DE LAS PERSONAS',
      'IDENTIFICAR EL VALOR DEL SERVICIO',
      'FORTALECER EL SERVICIO',
      'MEDICIÓN Y ANÁLISIS DE LA CALIDAD DEL SERVICIO',
      'LIDERAZGO Y COMPROMISO DE LA ALTA DIRECCIÓN',
      'CULTURA DE CALIDAD'
    ];
    public barChartType = 'bar';
    public barChartLegend = true;
  
    public barChartData: any[] = [
      {data: [65, 59, 80, 81, 56, 55], label: 'Puntaje máximo'},
      {data: [28, 48, 40, 19, 36, 27], label: 'Puntaje obtenido'}
    ];
  
    // Doughnut
    public doughnutChartLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
    public doughnutChartData: number[] = [350, 450, 100];
    public doughnutChartType = 'doughnut';
  
    // Radar
    public radarChartLabels: string[] = ['Eating', 'Drinking', 'Sleeping', 'Designing', 'Coding', 'Cycling', 'Running'];
  
    public radarChartData: any = [
      {data: [65, 59, 90, 81, 56, 55, 40], label: 'Series A'},
      {data: [28, 48, 40, 19, 96, 27, 100], label: 'Series B'}
    ];
    public radarChartType = 'radar';
  
    // Pie
    public pieChartLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail Sales'];
    public pieChartData: number[] = [300, 500, 100];
    public pieChartType = 'pie';
  
    // PolarArea
    public polarAreaChartLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail Sales', 'Telesales', 'Corporate Sales'];
    public polarAreaChartData: number[] = [300, 500, 100, 40, 120];
    public polarAreaLegend = true;
  
    public polarAreaChartType = 'polarArea';
  
    // events
    public chartClicked(e: any): void {
      console.log(e);
    }
  
    public chartHovered(e: any): void {
      console.log(e);
    }

    confirmarGuardar(){
      Swal.fire({
        title: '¿Está seguro de grabar el registro?',
        text: "Podrá modificarlo luego",
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#0a5d4c',
        cancelButtonColor: '#a2187e',
        confirmButtonText: 'Guardar'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Guardado',
            'El registro ha sido guardado',
            'success'
          )
        }
      });
     }
    
     confirmarFinalizar(){
      Swal.fire({
        title: '¿Está seguro de finalizar el registro?',
        text: "Ya no podrá modificarlo luego",
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#0a5d4c',
        cancelButtonColor: '#a2187e',
        confirmButtonText: 'Finalizar'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Finalizado',
            'El registro ha sido finalizado',
            'success'
          )
        }
      });
     }
    

}
