import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutodiagnosticoComponent } from './autodiagnostico/autodiagnostico.component';
import { InformeComponent } from './informe/informe.component';
import { AnalizarAutodiagnosticoComponent } from './analizar-autodiagnostico/analizar-autodiagnostico.component';
import { CronogramaComponent } from './cronograma/cronograma.component';
import {ActualizarCronogramaComponent} from './actualizar-cronograma/actualizar-cronograma.component';

const routes: Routes = 
[
  {
    path : '',
    data : {
      title : 'Planificación' //ruta principal despues del home, breadcrumb
    },
    children : [
        {
          path: '',
          redirectTo: 'Autodiagnostico' // ruta de breadcrumb principal
        },
        {
          path: 'Autodiagnostico', // ruta del componente en URL 
          component: AutodiagnosticoComponent ,
          data: {
            title: 'Autodiagnóstico' // nombre de breadrumb
          }
        },
        {
          path: 'AnalizarAutodiagnostico',
          component: AnalizarAutodiagnosticoComponent,
          data: {
            title: 'Analizar Autodiagnóstico '
          }
        },
        {
          path: 'ListCronograma',
          component: CronogramaComponent,
          data: {
            title: 'Lista de Cronogramas'
          }
        },
        {
          path: 'ActCronograma',
          component: ActualizarCronogramaComponent,
          data: {
            title: 'Actualizar'
          }
        }, 
        {
          path: 'Informe',
          component: InformeComponent,
          data: {
            title: 'Informe'
          }
        }
     
    ]
    
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanificacionRoutingModule { }
