import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-actualizar-cronograma',
  templateUrl: './actualizar-cronograma.component.html',
  styleUrls: ['./actualizar-cronograma.component.scss']
})
export class ActualizarCronogramaComponent implements OnInit {

  addForm: FormGroup;
  rowsComponent1: FormArray = this.fb.array([]);
  rowsComponent2: FormArray = this.fb.array([]);
  rowsComponent3: FormArray = this.fb.array([]);
  rowsComponent4: FormArray = this.fb.array([]);
  rowsComponent5: FormArray = this.fb.array([]);
  rowsComponent6: FormArray = this.fb.array([]);
  itemForm: FormGroup;

 // filaComponente1 = [{value: 'word1'}, {value: 'word2'}, {value: 'word3'}, {value: ''}]

  constructor(private fb: FormBuilder) {
  
   }

  ngOnInit(): void {
    this.initForm();
    this.rowsComponent1.push(this.createItemFormGroup());
    this.rowsComponent2.push(this.createItemFormGroup());
    this.rowsComponent3.push(this.createItemFormGroup());
    this.rowsComponent4.push(this.createItemFormGroup());
    this.rowsComponent5.push(this.createItemFormGroup());
    this.rowsComponent6.push(this.createItemFormGroup());
  }


  initForm(){
    this.addForm = this.fb.group({
    rows1 : this.rowsComponent1,
    rows2 : this.rowsComponent2,
    rows3 : this.rowsComponent3,
    rows4 : this.rowsComponent4,
    rows5 : this.rowsComponent5,
    rows6 : this.rowsComponent6
   });
}

  onAddRowComponent1() {
    this.rowsComponent1.push(this.createItemFormGroup());
  }
  onAddRowComponent2() {
    this.rowsComponent2.push(this.createItemFormGroup());
  }
  onAddRowComponent3() {
    this.rowsComponent3.push(this.createItemFormGroup());
  }
  onAddRowComponent4() {
    this.rowsComponent4.push(this.createItemFormGroup());
  }
  onAddRowComponent5() {
    this.rowsComponent5.push(this.createItemFormGroup());
  }
  onAddRowComponent6() {
    this.rowsComponent6.push(this.createItemFormGroup());
  }

  onRemoveRowComponent1(rowIndex:number){
    this.rowsComponent1.removeAt(rowIndex);
  }
  onRemoveRowComponent2(rowIndex:number){
    this.rowsComponent2.removeAt(rowIndex);
  }
  onRemoveRowComponent3(rowIndex:number){
    this.rowsComponent3.removeAt(rowIndex);
  }
  onRemoveRowComponent4(rowIndex:number){
    this.rowsComponent4.removeAt(rowIndex);
  }
  onRemoveRowComponent5(rowIndex:number){
    this.rowsComponent5.removeAt(rowIndex);
  }
  onRemoveRowComponent6(rowIndex:number){
    this.rowsComponent6.removeAt(rowIndex);
  }

  createItemFormGroup(): FormGroup {
    return this.fb.group({
      fm_Actividad: null,
      fm_Producto: null,
      fm_Responsable: null,
      fm_PorcAvance : null,
      // fm_Enero  : this.fb.array([this.semanas()]),
      // fm_Febrero  : this.fb.array([this.semanas()]),
      // fm_Marzo  : this.fb.array([this.semanas()]),
      // fm_Abril  : this.fb.array([this.semanas()]),
      // fm_Mayo  : this.fb.array([this.semanas()]),
      // fm_Junio  : this.fb.array([this.semanas()]),
      // fm_Julio  : this.fb.array([this.semanas()]),
      // fm_Agosto  : this.fb.array([this.semanas()]),
      // fm_Setiembre  : this.fb.array([this.semanas()]),
      // fm_Octubre  : this.fb.array([this.semanas()]),
      // fm_Noviembre  : this.fb.array([this.semanas()]),
      // fm_Diciembre  : this.fb.array([this.semanas()]),
    });
  }

  semanas():FormGroup{
    return this.fb.group({
      fm_s1: '',
      fm_s2: '',
      fm_s3: '',
      fm_s4 :''
    });
  }

  confirmarGuardar(){
    Swal.fire({
      title: '¿Está seguro de grabar el registro?',
      text: "Podrá modificarlo luego",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#0a5d4c',
      cancelButtonColor: '#a2187e',
      confirmButtonText: 'Guardar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Guardado',
          'El registro ha sido guardado',
          'success'
        )
      }
    });
   }
  
   confirmarFinalizar(){
    Swal.fire({
      title: '¿Está seguro de finalizar el registro?',
      text: "Ya no podrá modificarlo luego",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#0a5d4c',
      cancelButtonColor: '#a2187e',
      confirmButtonText: 'Finalizar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Finalizado',
          'El registro ha sido finalizado',
          'success'
        )
      }
    });
   }
  
}
