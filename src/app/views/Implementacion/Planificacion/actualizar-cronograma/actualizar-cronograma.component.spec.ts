import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActualizarCronogramaComponent } from './actualizar-cronograma.component';

describe('ActualizarCronogramaComponent', () => {
  let component: ActualizarCronogramaComponent;
  let fixture: ComponentFixture<ActualizarCronogramaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActualizarCronogramaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActualizarCronogramaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
