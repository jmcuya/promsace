import { Component, OnInit, ViewChild } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {NgbAccordionConfig} from '@ng-bootstrap/ng-bootstrap'
import {NgbPanelChangeEvent} from '@ng-bootstrap/ng-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { pdfDefaultOptions } from 'ngx-extended-pdf-viewer';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-informe',
  templateUrl: './informe.component.html',
  styleUrls: ['./informe.component.scss']
})
export class InformeComponent implements OnInit {

  @ViewChild('sendFileModal') public sendFileModal: ModalDirective;
  @ViewChild('attachments') attachment: any;
 

listFiles  :FileList;
Editor = ClassicEditor;
model1 =  { 
    editorData :  '<p> ¡Hola, mundo! </p>' 
} ; 
model2 =  { 
  editorData :  '<p> ¡Hola, mundo! </p>' 
} ; 
model3 =  { 
  editorData :  '<p> ¡Hola, mundo! </p>' 
} ; 
model4 =  { 
  editorData :  '<p> ¡Hola, mundo! </p>' 
} ; 
model5 =  { 
  editorData :  '<p> ¡Hola, mundo! </p>' 
} ; 


fileList: File[] = [];
listOfFiles: any[] = [];

document = "https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf";
  constructor(private _config:NgbAccordionConfig) {
    _config.closeOthers=false;
    pdfDefaultOptions.assetsFolder = 'bleeding-edge';
   }

  ngOnInit(): void {
  }

  public onReady( editor ) {
    editor.ui.getEditableElement().parentElement.insertBefore(
        editor.ui.view.toolbar.element,
        editor.ui.getEditableElement()
    );
    console.log("editor" ,editor)
}

ver(){
  console.log("modelo",this.model1)
}


nomodificar($event:NgbPanelChangeEvent){
  if ($event.panelId === 'panel1' && $event.nextState === false){
    $event.preventDefault();  }
  }

  openModal(){
    this.sendFileModal.show();
  }
  closeModal(){
    this.sendFileModal.hide();
  }

  uploadFile(event: Event) {
    const element = event.currentTarget as HTMLInputElement;
    let fileList: FileList | null = element.files;
    if (fileList) {
      this.listFiles= fileList;
      console.log("FileUpload -> files", fileList);
    }

  }

  onFileChanged(event: any) {
    for (var i = 0; i <= event.target.files.length - 1; i++) {
      var selectedFile = event.target.files[i];
      this.fileList.push(selectedFile);
      this.listOfFiles.push(selectedFile.name)
  }
  console.log("FileUpload -> files", this.listOfFiles);
  this.attachment.nativeElement.value = '';
} 

removeSelectedFile(index) {
  // Delete the item from fileNames list
  this.listOfFiles.splice(index, 1);
  // delete file from FileList
  this.fileList.splice(index, 1);
 }

 confirmarGuardar(){
  Swal.fire({
    title: '¿Está seguro de grabar el registro?',
    text: "Podrá modificarlo luego",
    icon: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonColor: '#0a5d4c',
    cancelButtonColor: '#a2187e',
    confirmButtonText: 'Guardar'
  }).then((result) => {
    if (result.isConfirmed) {
      Swal.fire(
        'Guardado',
        'El registro ha sido guardado',
        'success'
      )
    }
  });
 }

 confirmarFinalizar(){
  Swal.fire({
    title: '¿Está seguro de finalizar el registro?',
    text: "Ya no podrá modificarlo luego",
    icon: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonColor: '#0a5d4c',
    cancelButtonColor: '#a2187e',
    confirmButtonText: 'Finalizar'
  }).then((result) => {
    if (result.isConfirmed) {
      Swal.fire(
        'Finalizado',
        'El registro ha sido finalizado',
        'success'
      )
    }
  });
 }

}
