import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanificacionRoutingModule } from './planificacion-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { PdfViewerModule} from '@syncfusion/ej2-angular-pdfviewer';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { ModalModule } from 'ngx-bootstrap/modal';

import { CronogramaComponent } from './cronograma/cronograma.component';
import { AutodiagnosticoComponent } from './autodiagnostico/autodiagnostico.component';
import { AnalizarAutodiagnosticoComponent } from './analizar-autodiagnostico/analizar-autodiagnostico.component';
import { InformeComponent } from './informe/informe.component';
import { ActualizarCronogramaComponent } from './actualizar-cronograma/actualizar-cronograma.component';

//import { TablesComponent } from '../../base/tables.component';

@NgModule({
  declarations: [
    AutodiagnosticoComponent,
    AnalizarAutodiagnosticoComponent,
    CronogramaComponent,
    InformeComponent,
    ActualizarCronogramaComponent
  ],
  imports: [
    TabsModule,
    ChartsModule,
    CommonModule,
    PlanificacionRoutingModule,
    AccordionModule,
    FormsModule,
    ReactiveFormsModule,    
    CollapseModule.forRoot(),
    CommonModule,
    TabsModule,
    ChartsModule,
    CKEditorModule,
    PdfViewerModule,
    NgxExtendedPdfViewerModule,
    ModalModule,  
    AccordionModule,
    FormsModule,
    ReactiveFormsModule,
    //TablesComponent,
    //NgModule,    
    CollapseModule.forRoot(),

    PlanificacionRoutingModule,
  ]
})
export class PlanificacionModule { }
