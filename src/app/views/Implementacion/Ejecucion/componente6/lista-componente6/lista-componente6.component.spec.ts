import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaComponente6Component } from './lista-componente6.component';

describe('ListaComponente6Component', () => {
  let component: ListaComponente6Component;
  let fixture: ComponentFixture<ListaComponente6Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaComponente6Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaComponente6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
