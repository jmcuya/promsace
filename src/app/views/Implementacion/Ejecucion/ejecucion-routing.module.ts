import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CronogramaComponent } from './cronograma/cronograma.component';
import { ActualizarCronogramaComponent } from './actualizar-cronograma/actualizar-cronograma.component';


/* Componente 1 */
import { CaracterPersonasComponent } from './componente1/caracter-personas/caracter-personas.component';
//import { SegmentoPersonasComponent } from './componente1/segmento-personas/segmento-personas.component';
import { ConductoresComponent } from './componente1/conductores/conductores.component';
import { NecExpectComponent } from './componente1/nec-expect/nec-expect.component';
import { AgruparNecesidadesComponent } from './componente1/agrupar-necesidades/agrupar-necesidades.component';

import { ListaCaracterPersonaComponent } from './componente1/lista-caracter-persona/lista-caracter-persona.component';
import { ListaConductoresComponent } from './componente1/lista-conductores/lista-conductores.component';
import { ListaNecesidadesExpectativasComponent } from './componente1/lista-necesidades-expectativas/lista-necesidades-expectativas.component';
import { ListaAgrupacionNecesidadesComponent } from './componente1/lista-agrupacion-necesidades/lista-agrupacion-necesidades.component';

/* Componente 2 */
import {AnalisisDeProcesoComponent} from './componente2/analisis-de-proceso/analisis-de-proceso.component';
import {AnalisisEntornoComponent} from './componente2/analisis-entorno/analisis-entorno.component';
import {RevisionComparativaNecesidadesComponent } from './componente2/revision-comparativa-necesidades/revision-comparativa-necesidades.component';
//import {PropuestaValorPublicoComponent} from './componente2/propuesta-valor-publico/propuesta-valor-publico.component';
import {ValorPublicoServicioComponent} from './componente2/valor-publico-servicio/valor-publico-servicio.component';

import {ListaAnalisisProcesoComponent} from './componente2/lista-analisis-proceso/lista-analisis-proceso.component';
import {ListaAnalisisEntornoComponent} from './componente2/lista-analisis-entorno/lista-analisis-entorno.component';
import {ListaRevisionComparativaComponent} from './componente2/lista-revision-comparativa/lista-revision-comparativa.component';
import {ListaValorPublicoComponent} from './componente2/lista-valor-publico/lista-valor-publico.component';

/* Componente 3 */
import { EtapasProcesoServicioComponent } from './componente3/etapas-proceso-servicio/etapas-proceso-servicio.component';
import { AnalisisExperienciaComponent } from './componente3/analisis-experiencia/analisis-experiencia.component';
import { MapaExperienciaComponent } from './componente3/mapa-experiencia/mapa-experiencia.component';
import { DiagramaAfinidadComponent } from './componente3/diagrama-afinidad/diagrama-afinidad.component';
import { AnalisisProblemasComponent } from './componente3/analisis-problemas/analisis-problemas.component';
import { HerramientasAdicionalesComponent } from './componente3/herramientas-adicionales/herramientas-adicionales.component';

import {ListaEtapasProcesoComponent } from './componente3/lista-etapas-proceso/lista-etapas-proceso.component';
import {ListaMapaExperienciaComponent } from './componente3/lista-mapa-experiencia/lista-mapa-experiencia.component';
import {ListaDiagramaAfinidadComponent } from './componente3/lista-diagrama-afinidad/lista-diagrama-afinidad.component';
import {ListaAnalisisProblemaComponent } from './componente3/lista-analisis-problema/lista-analisis-problema.component';
import {ListaHerramientasAdicionalesComponent } from './componente3/lista-herramientas-adicionales/lista-herramientas-adicionales.component';

/* Componente 4 */
import { Componente4Component } from './componente4/componente4.component';
import { ListaComponente4Component} from './componente4/lista-componente4/lista-componente4.component';

/* Componente 5 */
import { Componente5Component } from './componente5/componente5.component';
import { ListaComponente5Component} from './componente5/lista-componente5/lista-componente5.component';

/* Componente 6 */
import { Componente6Component } from './componente6/componente6.component';
import {ListaComponente6Component } from './componente6/lista-componente6/lista-componente6.component';

import { InformeComponent } from './informe/informe.component';

const routes: Routes = 
[
  {
    path : '',
    data : {
      title : 'Ejecución' //ruta principal despues del home, breadcrumb
    },
    children : [
        {
          path: '',
          redirectTo: 'Cronograma' // ruta de breadcrumb principal
        },

            {
              path : 'Cronograma',
              component : CronogramaComponent,
              data:{
                title : 'Cronograma'
              }
            },
            {
              path : 'ActualizarCronograma',
              component : ActualizarCronogramaComponent,
              data:{
                title : 'Actualizar Cronograma'
              }
            },
        
        {
          path: '',        
          data: {
            title: 'Componente 1' 
          },
          children : [
            {
              path:'',
              redirectTo :'Conductores'
            },
            {
              path : 'CaractPersonas',
              component : CaracterPersonasComponent,
              data:{
                title : 'Caracterización de Personas'
              }
            },
            {
                path : 'Conductores',
                component : ConductoresComponent,
                data:{
                  title : 'Conductores'
                }
              },
              {
                path : 'NecExpectativas',
                component : NecExpectComponent,
                data:{
                  title : 'Necesidades y/o expectativas'
                }
              },
              {
                path : 'AgruparNecesidades',
                component : AgruparNecesidadesComponent,
                data:{
                  title : 'Agrupar necesidades y/o expectativas por Conductor'
                }
              },
              /*{
                path : 'ListaCaracterizacion',
                component : ListaCaracterPersonaComponent,
                data:{
                  title : 'Lista caracterización de Personas'
                }
              },
              {
                path : 'ListaConductores',
                component : ListaConductoresComponent,
                data:{
                  title : 'Lista conductores'
                }
              },
              {
                path : 'ListaNecesidadesExpectativas',
                component : ListaNecesidadesExpectativasComponent,
                data:{
                  title : 'Lista necesidades y/o expectativas'
                }
              } ,*/
              {
                path : 'ListaAgruparNecesidades',
                component : ListaAgrupacionNecesidadesComponent,
                data:{
                  title : 'Validacion de agrupaciones de necesidades y/o expectativas'
                }
              }          
          ]
        },

        {
          path: '',        
          data: {
            title: 'Componente 2' 
          },
          children : [
            {
              path:'',
              redirectTo :'AnalisisProcesos'
            },
             {
              path : 'AnalisisProcesos',
              component : AnalisisDeProcesoComponent,
              data:{
                title : 'Análisis del proceso'
              }
            },
            {
              path : 'AnalisisEntorno',
              component : AnalisisEntornoComponent,
              data:{
                title : 'Análisis del entorno'
              }
            },
            {
              path : 'RevisionComparativa',
              component : RevisionComparativaNecesidadesComponent,
              data:{
                title : 'Revisión comparativa de las necesidades y expectativas'
              }
            },
            {
              path : 'ValorPublicoServicio',
              component : ValorPublicoServicioComponent, //PropuestaValorPublicoComponent,
              data:{
                title : 'Propuesta de valor público del servicio'
              }
            },
            {
             path : 'ListaAnalisisProcesos',
             component : ListaAnalisisProcesoComponent,
             data:{
               title : 'Lista análisis del proceso'
             }
           },
           {
             path : 'ListaAnalisisEntorno',
             component : ListaAnalisisEntornoComponent,
             data:{
               title : 'Lista de análisis del entorno'
             }
           },
           {
             path : 'ListaRevisionComparativa',
             component : ListaRevisionComparativaComponent,
             data:{
               title : 'Lista de revisión comparativa de las necesidades y expectativas'
             }
           },
           {
             path : 'ListaValorPublicoServicio',
             component : ListaValorPublicoComponent, //PropuestaValorPublicoComponent,
             data:{
               title : 'Lista Propuesta de valor público del servicio'
             }
           }
            
          ]
        },

        {
          path: '',        
          data: {
            title: 'Componente 3' 
          },
          children : [
            {
              path:'',
              redirectTo :'MapExperiencia'
            },           
            {
              path : 'MapExperiencia',
              component : MapaExperienciaComponent,
              data:{
                title : 'Mapa de experiencia'
              }
            },
            {
              path : 'DiaAfinidad',
              component : DiagramaAfinidadComponent,
              data:{
                title : 'Diagrama de afinidad'
              }
            },
            {
              path : 'AnalisisProblemas',
              component : AnalisisProblemasComponent,
              data:{
                title : 'Análisis de problemas'
              }
            },
            {
              path : 'HerramientasAdicionales',
              component : HerramientasAdicionalesComponent,
              data:{
                title : 'Herramientas Adicionales'
              }
            },
            {
              path : 'ListaMapaExperiencia',
              component : ListaMapaExperienciaComponent,
              data:{
                title : 'Lista Mapa de Experiencia'
              }
            },
            {
              path : 'ListaDiagramaAfinidad',
              component : ListaDiagramaAfinidadComponent,
              data:{
                title : 'Lista Diagrama de afinidad'
              }
            },
            {
              path : 'ListaAnalisisProblemas',
              component : ListaAnalisisProblemaComponent,
              data:{
                title : 'Lista AnalisisProblemas'
              }
            },
            {
              path : 'ListaHerramientasAdicionales',
              component : ListaHerramientasAdicionalesComponent,
              data:{
                title : 'Lista Herramientas Adicionales'
              }
            }
          ]
        },
        {
          path : 'Componente4',
          component : Componente4Component,
          data:{
            title : 'Componente 4'
          }
        },
        {
          path : 'ListaComponente4',
          component : ListaComponente4Component,
          data:{
            title : 'Lista de registro Componente 4'
          }
        },
        {
          path : 'Componente5',
          component : Componente5Component,
          data:{
            title : 'Componente 5'
          }
        },
        {
          path : 'ListaComponente5',
          component : ListaComponente5Component,
          data:{
            title : 'Lista de registro Componente 5'
          }
        },        
        {
          path : 'Componente6',
          component : Componente6Component,
          data:{
            title : 'Componente 6'
          }
        },
        {
          path : 'ListaComponente6',
          component : ListaComponente6Component,
          data:{
            title : 'Lista de registro Componente 6'
          }
        },
        {
          path : 'Informe',
          component : InformeComponent,
          data:{
            title : 'Informe'
          }
        },
    ]
    
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EjecucionRoutingModule { }
