import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgruparNecesidadesComponent } from './agrupar-necesidades.component';

describe('AgruparNecesidadesComponent', () => {
  let component: AgruparNecesidadesComponent;
  let fixture: ComponentFixture<AgruparNecesidadesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgruparNecesidadesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgruparNecesidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
