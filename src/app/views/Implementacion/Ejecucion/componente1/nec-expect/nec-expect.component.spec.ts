import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NecExpectComponent } from './nec-expect.component';

describe('NecExpectComponent', () => {
  let component: NecExpectComponent;
  let fixture: ComponentFixture<NecExpectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NecExpectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NecExpectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
