import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SegmentoPersonasComponent } from './segmento-personas.component';

describe('SegmentoPersonasComponent', () => {
  let component: SegmentoPersonasComponent;
  let fixture: ComponentFixture<SegmentoPersonasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SegmentoPersonasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SegmentoPersonasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
