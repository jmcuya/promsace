import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaracterPersonasComponent } from './caracter-personas.component';

describe('CaracterPersonasComponent', () => {
  let component: CaracterPersonasComponent;
  let fixture: ComponentFixture<CaracterPersonasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaracterPersonasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaracterPersonasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
