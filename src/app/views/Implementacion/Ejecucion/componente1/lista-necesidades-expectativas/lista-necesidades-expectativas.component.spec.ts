import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaNecesidadesExpectativasComponent } from './lista-necesidades-expectativas.component';

describe('ListaNecesidadesExpectativasComponent', () => {
  let component: ListaNecesidadesExpectativasComponent;
  let fixture: ComponentFixture<ListaNecesidadesExpectativasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaNecesidadesExpectativasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaNecesidadesExpectativasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
