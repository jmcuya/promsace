import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCaracterPersonaComponent } from './lista-caracter-persona.component';

describe('ListaCaracterPersonaComponent', () => {
  let component: ListaCaracterPersonaComponent;
  let fixture: ComponentFixture<ListaCaracterPersonaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaCaracterPersonaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCaracterPersonaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
