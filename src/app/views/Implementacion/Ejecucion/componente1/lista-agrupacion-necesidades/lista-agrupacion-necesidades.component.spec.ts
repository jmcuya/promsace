import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaAgrupacionNecesidadesComponent } from './lista-agrupacion-necesidades.component';

describe('ListaAgrupacionNecesidadesComponent', () => {
  let component: ListaAgrupacionNecesidadesComponent;
  let fixture: ComponentFixture<ListaAgrupacionNecesidadesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaAgrupacionNecesidadesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaAgrupacionNecesidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
