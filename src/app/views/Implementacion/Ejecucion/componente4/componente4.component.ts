import { Component, OnInit, ViewChild } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {NgbAccordionConfig} from '@ng-bootstrap/ng-bootstrap'
import {NgbPanelChangeEvent} from '@ng-bootstrap/ng-bootstrap';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { pdfDefaultOptions } from 'ngx-extended-pdf-viewer';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-componente4',
  templateUrl: './componente4.component.html',
  styleUrls: ['./componente4.component.scss']
})
export class Componente4Component implements OnInit {
  
  
  @ViewChild('sendFileModal') public sendFileModal: ModalDirective;
  @ViewChild('attachments') attachment: any;
 

  listFiles  :FileList;
   Editor = ClassicEditor;
   model1 =  { 
    editorData :  '<p> ¡Hola, mundo! </p>' 
} ;   


  ngOnInit(): void {
  }

  public onReady( editor ) {
    editor.ui.getEditableElement().parentElement.insertBefore(
        editor.ui.view.toolbar.element,
        editor.ui.getEditableElement()
    );
    console.log("editor" ,editor)
}

ver(){
  console.log("modelo",this.model1)
}

confirmarGuardar(){
  Swal.fire({
    title: '¿Está seguro de grabar el registro?',
    text: "Podrá modificarlo luego",
    icon: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonColor: '#0a5d4c',
    cancelButtonColor: '#a2187e',
    confirmButtonText: 'Guardar'
  }).then((result) => {
    if (result.isConfirmed) {
      Swal.fire(
        'Guardado',
        'El registro ha sido guardado',
        'success'
      )
    }
  });
 }

 confirmarFinalizar(){
  Swal.fire({
    title: '¿Está seguro de finalizar el registro?',
    text: "Ya no podrá modificarlo luego",
    icon: 'warning',
    showCancelButton: true,
    cancelButtonText: 'Cancelar',
    confirmButtonColor: '#0a5d4c',
    cancelButtonColor: '#a2187e',
    confirmButtonText: 'Finalizar'
  }).then((result) => {
    if (result.isConfirmed) {
      Swal.fire(
        'Finalizado',
        'El registro ha sido finalizado',
        'success'
      )
    }
  });
 }

}
