import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaComponente4Component } from './lista-componente4.component';

describe('ListaComponente4Component', () => {
  let component: ListaComponente4Component;
  let fixture: ComponentFixture<ListaComponente4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaComponente4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaComponente4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
