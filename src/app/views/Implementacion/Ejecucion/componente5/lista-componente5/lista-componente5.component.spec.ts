import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaComponente5Component } from './lista-componente5.component';

describe('ListaComponente5Component', () => {
  let component: ListaComponente5Component;
  let fixture: ComponentFixture<ListaComponente5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaComponente5Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaComponente5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
