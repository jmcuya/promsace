import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaEtapasProcesoComponent } from './lista-etapas-proceso.component';

describe('ListaEtapasProcesoComponent', () => {
  let component: ListaEtapasProcesoComponent;
  let fixture: ComponentFixture<ListaEtapasProcesoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaEtapasProcesoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaEtapasProcesoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
