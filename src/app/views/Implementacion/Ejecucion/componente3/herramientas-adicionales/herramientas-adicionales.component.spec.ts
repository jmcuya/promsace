import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HerramientasAdicionalesComponent } from './herramientas-adicionales.component';

describe('HerramientasAdicionalesComponent', () => {
  let component: HerramientasAdicionalesComponent;
  let fixture: ComponentFixture<HerramientasAdicionalesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HerramientasAdicionalesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HerramientasAdicionalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
