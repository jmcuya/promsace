import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaAnalisisProblemaComponent } from './lista-analisis-problema.component';

describe('ListaAnalisisProblemaComponent', () => {
  let component: ListaAnalisisProblemaComponent;
  let fixture: ComponentFixture<ListaAnalisisProblemaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaAnalisisProblemaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaAnalisisProblemaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
