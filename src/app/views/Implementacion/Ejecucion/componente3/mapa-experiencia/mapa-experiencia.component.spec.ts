import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapaExperienciaComponent } from './mapa-experiencia.component';

describe('MapaExperienciaComponent', () => {
  let component: MapaExperienciaComponent;
  let fixture: ComponentFixture<MapaExperienciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapaExperienciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapaExperienciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
