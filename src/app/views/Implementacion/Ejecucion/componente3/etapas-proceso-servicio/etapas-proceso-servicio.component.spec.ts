import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtapasProcesoServicioComponent } from './etapas-proceso-servicio.component';

describe('EtapasProcesoServicioComponent', () => {
  let component: EtapasProcesoServicioComponent;
  let fixture: ComponentFixture<EtapasProcesoServicioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtapasProcesoServicioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtapasProcesoServicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
