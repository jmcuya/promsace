import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaHerramientasAdicionalesComponent } from './lista-herramientas-adicionales.component';

describe('ListaHerramientasAdicionalesComponent', () => {
  let component: ListaHerramientasAdicionalesComponent;
  let fixture: ComponentFixture<ListaHerramientasAdicionalesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaHerramientasAdicionalesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaHerramientasAdicionalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
