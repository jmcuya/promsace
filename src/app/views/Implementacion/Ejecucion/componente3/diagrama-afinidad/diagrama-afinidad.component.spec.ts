import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DiagramaAfinidadComponent } from './diagrama-afinidad.component';

describe('DiagramaAfinidadComponent', () => {
  let component: DiagramaAfinidadComponent;
  let fixture: ComponentFixture<DiagramaAfinidadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DiagramaAfinidadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagramaAfinidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
