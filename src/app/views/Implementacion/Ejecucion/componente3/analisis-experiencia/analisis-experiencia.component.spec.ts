import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalisisExperienciaComponent } from './analisis-experiencia.component';

describe('AnalisisExperienciaComponent', () => {
  let component: AnalisisExperienciaComponent;
  let fixture: ComponentFixture<AnalisisExperienciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnalisisExperienciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalisisExperienciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
