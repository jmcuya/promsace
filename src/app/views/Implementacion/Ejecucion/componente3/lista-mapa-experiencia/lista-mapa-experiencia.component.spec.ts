import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaMapaExperienciaComponent } from './lista-mapa-experiencia.component';

describe('ListaMapaExperienciaComponent', () => {
  let component: ListaMapaExperienciaComponent;
  let fixture: ComponentFixture<ListaMapaExperienciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaMapaExperienciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaMapaExperienciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
