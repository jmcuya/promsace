import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaDiagramaAfinidadComponent } from './lista-diagrama-afinidad.component';

describe('ListaDiagramaAfinidadComponent', () => {
  let component: ListaDiagramaAfinidadComponent;
  let fixture: ComponentFixture<ListaDiagramaAfinidadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaDiagramaAfinidadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaDiagramaAfinidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
