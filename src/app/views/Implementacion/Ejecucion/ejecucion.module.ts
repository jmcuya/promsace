import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EjecucionRoutingModule } from './ejecucion-routing.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { PdfViewerModule} from '@syncfusion/ej2-angular-pdfviewer';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CollapseModule } from 'ngx-bootstrap/collapse';


import { CronogramaComponent } from './cronograma/cronograma.component';

import { AgruparNecesidadesComponent } from './componente1/agrupar-necesidades/agrupar-necesidades.component';
import { CaracterPersonasComponent } from './componente1/caracter-personas/caracter-personas.component';
import { ConductoresComponent } from './componente1/conductores/conductores.component';
import { NecExpectComponent } from './componente1/nec-expect/nec-expect.component';
import { SegmentoPersonasComponent } from './componente1/segmento-personas/segmento-personas.component';

import { AnalisisDeProcesoComponent } from './componente2/analisis-de-proceso/analisis-de-proceso.component';
import { AnalisisEntornoComponent } from './componente2/analisis-entorno/analisis-entorno.component';
import { RevisionComparativaNecesidadesComponent } from './Componente2/revision-comparativa-necesidades/revision-comparativa-necesidades.component';
import { ValorPublicoServicioComponent } from './Componente2/valor-publico-servicio/valor-publico-servicio.component';


import { AnalisisExperienciaComponent } from './Componente3/analisis-experiencia/analisis-experiencia.component';
import { AnalisisProblemasComponent } from './componente3/analisis-problemas/analisis-problemas.component';
import { DiagramaAfinidadComponent } from './componente3/diagrama-afinidad/diagrama-afinidad.component';
import { EtapasProcesoServicioComponent } from './Componente3/etapas-proceso-servicio/etapas-proceso-servicio.component';
import { MapaExperienciaComponent } from './componente3/mapa-experiencia/mapa-experiencia.component';

import { Componente4Component } from './componente4/componente4.component';

import { Componente5Component } from './componente5/componente5.component';

import { Componente6Component } from './componente6/componente6.component';

import { InformeComponent } from './informe/informe.component';
import { ListaCaracterPersonaComponent } from './componente1/lista-caracter-persona/lista-caracter-persona.component';
import { ListaConductoresComponent } from './componente1/lista-conductores/lista-conductores.component';
import { ListaNecesidadesExpectativasComponent } from './componente1/lista-necesidades-expectativas/lista-necesidades-expectativas.component';
import { ListaAgrupacionNecesidadesComponent } from './componente1/lista-agrupacion-necesidades/lista-agrupacion-necesidades.component';
import { ListaAnalisisProcesoComponent } from './componente2/lista-analisis-proceso/lista-analisis-proceso.component';
import { ListaAnalisisEntornoComponent } from './componente2/lista-analisis-entorno/lista-analisis-entorno.component';
import { ListaRevisionComparativaComponent } from './componente2/lista-revision-comparativa/lista-revision-comparativa.component';
import { ListaValorPublicoComponent } from './componente2/lista-valor-publico/lista-valor-publico.component';
import { ListaEtapasProcesoComponent } from './componente3/lista-etapas-proceso/lista-etapas-proceso.component';
import { ListaComponente4Component } from './componente4/lista-componente4/lista-componente4.component';
import { ListaComponente5Component } from './componente5/lista-componente5/lista-componente5.component';
import { ListaComponente6Component } from './componente6/lista-componente6/lista-componente6.component';
import { ListaMapaExperienciaComponent } from './componente3/lista-mapa-experiencia/lista-mapa-experiencia.component';
import { ListaDiagramaAfinidadComponent } from './componente3/lista-diagrama-afinidad/lista-diagrama-afinidad.component';
import { ListaAnalisisProblemaComponent } from './componente3/lista-analisis-problema/lista-analisis-problema.component';
import { HerramientasAdicionalesComponent } from './componente3/herramientas-adicionales/herramientas-adicionales.component';
import { ListaHerramientasAdicionalesComponent } from './componente3/lista-herramientas-adicionales/lista-herramientas-adicionales.component';
import { ActualizarCronogramaComponent } from './actualizar-cronograma/actualizar-cronograma.component';

//import { PropuestaValorPublicoComponent } from './Componente2/propuesta-valor-publico/propuesta-valor-publico.component';



@NgModule({
  declarations: [
    CronogramaComponent,
    CaracterPersonasComponent,
    SegmentoPersonasComponent,
    ConductoresComponent,
    NecExpectComponent,  
    MapaExperienciaComponent,
    DiagramaAfinidadComponent,
    AnalisisProblemasComponent,
    Componente4Component,
    Componente5Component,
    InformeComponent,
    Componente6Component,
    AgruparNecesidadesComponent,
    RevisionComparativaNecesidadesComponent,
    ValorPublicoServicioComponent,
    EtapasProcesoServicioComponent,
    AnalisisExperienciaComponent,     
    AnalisisDeProcesoComponent,
    AnalisisEntornoComponent,
    ListaCaracterPersonaComponent,
    ListaConductoresComponent,
    ListaNecesidadesExpectativasComponent,
    ListaAgrupacionNecesidadesComponent,
    ListaAnalisisProcesoComponent,
    ListaAnalisisEntornoComponent,
    ListaRevisionComparativaComponent,
    ListaValorPublicoComponent,
    ListaEtapasProcesoComponent,
    ListaComponente4Component,
    ListaComponente5Component,
    ListaComponente6Component,
    ListaMapaExperienciaComponent,
    ListaDiagramaAfinidadComponent,
    ListaAnalisisProblemaComponent,
    HerramientasAdicionalesComponent,
    ListaHerramientasAdicionalesComponent,
    ActualizarCronogramaComponent
  ],
  imports: [
    CommonModule,
    CKEditorModule,
    PdfViewerModule,
    NgxExtendedPdfViewerModule ,
    ModalModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule,
    ChartsModule,
    AccordionModule,
    CollapseModule,
    
    EjecucionRoutingModule
  ]
})
export class EjecucionModule { }
