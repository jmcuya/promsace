import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaAnalisisProcesoComponent } from './lista-analisis-proceso.component';

describe('ListaAnalisisProcesoComponent', () => {
  let component: ListaAnalisisProcesoComponent;
  let fixture: ComponentFixture<ListaAnalisisProcesoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaAnalisisProcesoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaAnalisisProcesoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
