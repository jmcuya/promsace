import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaValorPublicoComponent } from './lista-valor-publico.component';

describe('ListaValorPublicoComponent', () => {
  let component: ListaValorPublicoComponent;
  let fixture: ComponentFixture<ListaValorPublicoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaValorPublicoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaValorPublicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
