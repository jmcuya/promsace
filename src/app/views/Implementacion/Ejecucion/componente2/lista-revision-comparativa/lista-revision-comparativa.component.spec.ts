import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaRevisionComparativaComponent } from './lista-revision-comparativa.component';

describe('ListaRevisionComparativaComponent', () => {
  let component: ListaRevisionComparativaComponent;
  let fixture: ComponentFixture<ListaRevisionComparativaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaRevisionComparativaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaRevisionComparativaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
