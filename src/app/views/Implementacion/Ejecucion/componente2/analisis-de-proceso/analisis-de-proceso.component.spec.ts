import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalisisDeProcesoComponent } from './analisis-de-proceso.component';

describe('AnalisisDeProcesoComponent', () => {
  let component: AnalisisDeProcesoComponent;
  let fixture: ComponentFixture<AnalisisDeProcesoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnalisisDeProcesoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalisisDeProcesoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
