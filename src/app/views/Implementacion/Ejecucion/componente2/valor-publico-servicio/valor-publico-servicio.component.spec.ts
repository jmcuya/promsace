import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValorPublicoServicioComponent } from './valor-publico-servicio.component';

describe('ValorPublicoServicioComponent', () => {
  let component: ValorPublicoServicioComponent;
  let fixture: ComponentFixture<ValorPublicoServicioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ValorPublicoServicioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ValorPublicoServicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
