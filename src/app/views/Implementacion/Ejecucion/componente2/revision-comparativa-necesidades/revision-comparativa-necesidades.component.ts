import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-revision-comparativa-necesidades',
  templateUrl: './revision-comparativa-necesidades.component.html',
  styleUrls: ['./revision-comparativa-necesidades.component.scss']
})
export class RevisionComparativaNecesidadesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  confirmarGuardar(){
    Swal.fire({
      title: '¿Está seguro de grabar el registro?',
      text: "Podrá modificarlo luego",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#0a5d4c',
      cancelButtonColor: '#a2187e',
      confirmButtonText: 'Guardar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Guardado',
          'El registro ha sido guardado',
          'success'
        )
      }
    });
   }
  
   confirmarFinalizar(){
    Swal.fire({
      title: '¿Está seguro de finalizar el registro?',
      text: "Ya no podrá modificarlo luego",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#0a5d4c',
      cancelButtonColor: '#a2187e',
      confirmButtonText: 'Finalizar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Finalizado',
          'El registro ha sido finalizado',
          'success'
        )
      }
    });
   }
  
}
