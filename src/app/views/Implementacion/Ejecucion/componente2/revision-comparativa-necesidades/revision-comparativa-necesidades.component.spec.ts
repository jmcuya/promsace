import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RevisionComparativaNecesidadesComponent } from './revision-comparativa-necesidades.component';

describe('RevisionComparativaNecesidadesComponent', () => {
  let component: RevisionComparativaNecesidadesComponent;
  let fixture: ComponentFixture<RevisionComparativaNecesidadesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RevisionComparativaNecesidadesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisionComparativaNecesidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
