import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaAnalisisEntornoComponent } from './lista-analisis-entorno.component';

describe('ListaAnalisisEntornoComponent', () => {
  let component: ListaAnalisisEntornoComponent;
  let fixture: ComponentFixture<ListaAnalisisEntornoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaAnalisisEntornoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaAnalisisEntornoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
