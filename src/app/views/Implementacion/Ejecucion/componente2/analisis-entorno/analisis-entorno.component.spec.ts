import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalisisEntornoComponent } from './analisis-entorno.component';

describe('AnalisisEntornoComponent', () => {
  let component: AnalisisEntornoComponent;
  let fixture: ComponentFixture<AnalisisEntornoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnalisisEntornoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalisisEntornoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
