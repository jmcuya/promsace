import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionSecuancialComponent } from './gestion-secuancial.component';

describe('GestionSecuancialComponent', () => {
  let component: GestionSecuancialComponent;
  let fixture: ComponentFixture<GestionSecuancialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionSecuancialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionSecuancialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
