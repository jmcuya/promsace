import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionSectoristaComponent } from './gestion-sectorista.component';

describe('GestionSectoristaComponent', () => {
  let component: GestionSectoristaComponent;
  let fixture: ComponentFixture<GestionSectoristaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionSectoristaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionSectoristaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
