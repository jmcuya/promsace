import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MonitoreoRoutingModule } from './monitoreo-routing.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { PdfViewerModule} from '@syncfusion/ej2-angular-pdfviewer';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { ModalModule } from 'ngx-bootstrap/modal';

import { CronogramaComponent } from './cronograma/cronograma.component';
import { GestionSecuancialComponent } from './gestion-secuancial/gestion-secuancial.component';
import { GestionSectoristaComponent } from './gestion-sectorista/gestion-sectorista.component';
import { InformeComponent } from './informe/informe.component';


@NgModule({
  declarations: [
    CronogramaComponent,
    GestionSecuancialComponent,
    GestionSectoristaComponent,
    InformeComponent
  ],
  imports: [
    CommonModule,
    CKEditorModule,
    PdfViewerModule,
    NgxExtendedPdfViewerModule, 
    ModalModule,
    
    MonitoreoRoutingModule
  ]
})
export class MonitoreoModule { }
