import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CronogramaComponent } from './cronograma/cronograma.component';
import { GestionSecuancialComponent } from './gestion-secuancial/gestion-secuancial.component';
import { GestionSectoristaComponent } from './gestion-sectorista/gestion-sectorista.component';
import { InformeComponent } from './informe/informe.component';

const routes: Routes = 
[
  {
    path : '',
    data : {
      title : 'Monitoreo' //ruta principal despues del home, breadcrumb
    },
    children : [
        {
          path: '',
          redirectTo: 'Cronograma' // ruta de breadcrumb principal
        },
        {
          path: 'Cronograma', // ruta del componente en URL 
          component: CronogramaComponent ,
          data: {
            title: 'Cronograma' // nombre de breadrumb
          }
        },
        {
          path: 'GestSecuencial',
          component: GestionSecuancialComponent,
          data: {
            title: 'Gestionar Secuencialidad'
          }
        },
        {
          path: 'GestSectorista',
          component: GestionSectoristaComponent,
          data: {
            title: 'Gestionar Secorista'
          }
        },
        {
          path: 'Informe',
          component: InformeComponent,
          data: {
            title: 'Informe Semestral de Monitoreo'
          }
        }
     
    ]
    
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonitoreoRoutingModule { }
