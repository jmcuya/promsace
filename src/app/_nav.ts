import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer'
  
  },
  {
    title: true,
    name: 'Implementación'
  },
  {
    name: 'Condiciones Previas',
    url: '/Implementacion',
    icon: 'icon-layers',
    children: [ 
      {
        name: 'Evaluar Entidad',
        url: 'Implementacion/CondicionesPrevias/EvaluaEntidad',
        icon: 'icon-people'
      },
      {
        name: 'Designar Maxima Autoridad',
        url: 'Implementacion/CondicionesPrevias/DesignarMaximaAutoridad',
        icon: 'icon-note'
      },     
      {
        name: 'Designar Responsable',
        url: 'Implementacion/CondicionesPrevias/DesignarResponsable',
        icon: 'icon-note'
      },
      {
        name: 'Equipos de trabajo',
        url: 'Implementacion/CondicionesPrevias/EquipoTrabajo',
        icon: 'icon-note'
      },
      {
        name: 'Nuevo miembro de equipo',
        url: 'Implementacion/CondicionesPrevias/NuevoIntegranteEquipoTrabajo',
        icon: 'icon-note'
      },      
      
      {
        name: 'Criterios de priorización',
        url: 'Implementacion/CondicionesPrevias/CritDePriorizacion',
        icon: 'icon-note'
      },
      {
        name: 'Configurar criterios de priorización',
        url: 'Implementacion/CondicionesPrevias/CritPriorizacion',
        icon: 'icon-note'
      },
      {
        name: 'Servicios',
        url: 'Implementacion/CondicionesPrevias/Servicios',
        icon: 'icon-note'
      },
      {
        name: 'Nuevo Servicio - Priorización',
        url: 'Implementacion/CondicionesPrevias/NuevoServicioPriorizacion',
        icon: 'icon-note'
      },     
      {
        name: 'Informe',
        url: 'Implementacion/CondicionesPrevias/Informe',
        icon: 'icon-note'
      }
    ]
  },
 
  {
    name: 'Planificación',
    url: '/Implementacion',
    icon: 'icon-layers',
    children: [
      {
        name: 'Autodiagnóstico',
        url: 'Implementacion/Planificacion/Autodiagnostico',
        icon: 'icon-drop'
      },
      {
        name: 'Analizar Autodiagnóstico',
        url: 'Implementacion/Planificacion/AnalizarAutodiagnostico',
        icon: 'icon-drop'
      },
      {
        name: 'Lista de Cronogramas',
        url: 'Implementacion/Planificacion/ListCronograma',
        icon: 'icon-drop'
      },
      
      {
        name: 'Actualizar Cronograma',
        url: 'Implementacion/Planificacion/ActCronograma',
        icon: 'icon-drop'
      },
      {
        name: 'Informe',
        url: 'Implementacion/Planificacion/Informe',
        icon: 'icon-drop'
      }
    ]
  },
  {
    name: 'Ejecución',
    url: '/Implementacion',
    icon: 'icon-layers',
    children: [
      {
        name: 'Cronograma',
        url: '/Implementacion/Ejecucion/Cronograma',
        icon: 'icon-drop'
      },
      {
        name: 'ActualizarCronograma',
        url: '/Implementacion/Ejecucion/ActualizarCronograma',
        icon: 'icon-drop'
      },
      {
        name: 'Componente 1',
        url: '/Implementacion',
        icon: 'icon-drop',
        children :[
          {
            name: 'Caracterización de personas',
            url : '/Implementacion/Ejecucion/CaractPersonas',
            icon: 'icon-drop'
          },
          /*{
            name: 'Segmentos de personas',
            url:'/Implementacion/Ejecucion/SegmentoPersonas',
            icon : 'icon-drop'
          }*/
          {
            name: 'Definir Conductores',
            url : '/Implementacion/Ejecucion/Conductores',
            icon: 'icon-drop'
          },
          {
            name: 'Definir Necesidades y/o expectativas',
            url:'/Implementacion/Ejecucion/NecExpectativas',
            icon : 'icon-drop'
          },
          {
            name: 'Agrupar necesidades y/o expectativas',
            url:'/Implementacion/Ejecucion/AgruparNecesidades',
            icon : 'icon-drop'
          },
          /*LISTAS*/
          /*{
            name: 'Lista de caracterización de personas',
            url : '/Implementacion/Ejecucion/ListaCaracterizacion',
            icon: 'icon-drop'
          },
          {
            name: 'Lista de conductores',
            url : '/Implementacion/Ejecucion/ListaConductores',
            icon: 'icon-drop'
          },
          {
            name: 'Lista necesidades y/o expectativas',
            url:'/Implementacion/Ejecucion/ListaNecesidadesExpectativas',
            icon : 'icon-drop'
          },*/
          {
            name: 'Necesidades y/o expectativas',
            url:'/Implementacion/Ejecucion/ListaAgruparNecesidades',
            icon : 'icon-drop'
          }

          ]
      },
      {
        name: 'Componente 2',
        url: '/Implementacion',
        icon: 'icon-drop',
        children :[
          {
            name: 'Registrar Análisis del procesos',
            url : '/Implementacion/Ejecucion/AnalisisProcesos',
            icon: 'icon-drop'
          },
          {
            name: 'Registrar Análisis del entorno',
            url : '/Implementacion/Ejecucion/AnalisisEntorno',
            icon: 'icon-drop'
          },
          {
            name: 'Registrar Revisión Comparativa',
            url : '/Implementacion/Ejecucion/RevisionComparativa',
            icon: 'icon-drop'
          },
          {
            name: 'Definir Propuestas de Valor Publico',
            url : '/Implementacion/Ejecucion/ValorPublicoServicio',
            icon: 'icon-drop'
          },
          /*listas*/
          {
            name: 'Análisis del procesos',
            url : '/Implementacion/Ejecucion/ListaAnalisisProcesos',
            icon: 'icon-drop'
          },
          {
            name: 'Análisis del entorno',
            url : '/Implementacion/Ejecucion/ListaAnalisisEntorno',
            icon: 'icon-drop'
          },
          {
            name: 'Revisión Comparativa',
            url : '/Implementacion/Ejecucion/ListaRevisionComparativa',
            icon: 'icon-drop'
          },
          {
            name: 'propuestas de Valor Publico',
            url : '/Implementacion/Ejecucion/ListaValorPublicoServicio',
            icon: 'icon-drop'
          }
          ]
      },
      {
        name: 'Componente 3',
        url: '/Implementacion',
        icon: 'icon-drop',
        children :[   
         /* {
            name: 'Etapas del proceso de servicio',
            url:'/Implementacion/Ejecucion/EtapasProcesoServicio',
            icon : 'icon-drop'
          },
          {
            name: 'Análisis de la Experiencia',
            url:'/Implementacion/Ejecucion/AnalisisExperiencia',
            icon : 'icon-drop'
          },*/
          {
            name: 'Registrar Información de Mapa de experiencia',
            url:'/Implementacion/Ejecucion/MapExperiencia',
            icon : 'icon-drop'
          },
          {
            name: 'Registrar Información de Diagrama de afinidad',
            url:'/Implementacion/Ejecucion/DiaAfinidad',
            icon : 'icon-drop'
          },
          {
            name: 'Registrar Información de Análisis de problema',
            url:'/Implementacion/Ejecucion/AnalisisProblemas',
            icon : 'icon-drop'
          },
          {
            name: 'Registrar Información de Herramientas Adicionales',
            url:'/Implementacion/Ejecucion/HerramientasAdicionales',
            icon : 'icon-drop'
          },
          {
            name: 'Mapa de experiencia',
            url:'/Implementacion/Ejecucion/ListaMapaExperiencia',
            icon : 'icon-drop'
          },
          {
            name: 'Diagrama de afinidad',
            url:'/Implementacion/Ejecucion/ListaDiagramaAfinidad',
            icon : 'icon-drop'
          },
          {
            name: 'Análisis de problema',
            url:'/Implementacion/Ejecucion/ListaAnalisisProblemas',
            icon : 'icon-drop'
          },
          {
            name: 'Herramientas Adicionales',
            url:'/Implementacion/Ejecucion/ListaHerramientasAdicionales',
            icon : 'icon-drop'
          }
          
          ]
      },
      {
        name: 'Componente 4',
        url: '/Implementacion',
        icon: 'icon-drop',
        children :[
          {
            name: 'Registrar Componente4',
            url: '/Implementacion/Ejecucion/Componente4',
            icon: 'icon-drop'
          },
          {
            name: 'Componente 4',
            url: '/Implementacion/Ejecucion/ListaComponente4',
            icon: 'icon-drop'
          }
        ]
      },   
      {
        name: 'Componente 5',
        url: '/Implementacion',
        icon: 'icon-drop',
        children :[
          {
            name: 'Registrar Componente5',
            url: '/Implementacion/Ejecucion/Componente5',
            icon: 'icon-drop'
          },
          {
            name: 'Componente 5',
            url: '/Implementacion/Ejecucion/ListaComponente5',
            icon: 'icon-drop'
          }
        ]
      }, 
      {
        name: 'Componente 6',
        url: '/Implementacion',
        icon: 'icon-drop',
        children :[
          {
            name: 'Registrar Componente6',
            url: '/Implementacion/Ejecucion/Componente6',
            icon: 'icon-drop'
          },
          {
            name: 'Componente 6',
            url: '/Implementacion/Ejecucion/ListaComponente6',
            icon: 'icon-drop'
          }
        ]
      },         
      
      {
        name: 'Informe',
        url: '/Implementacion/Ejecucion/Informe',
        icon: 'icon-drop'
      }
    ]
  },
  {
    name: 'Gestionar Entidades',
    url: '/Entidades',
    icon: 'icon-layers',
    children: [
      {
        name: 'Gestionar Entidad',
        url: '/Entidades/GestionarEntidad',
        icon: 'icon-drop'
      }
    ]
  },
  {
    name: 'Monitoreo',
    url: '/Implementacion',
    icon: 'icon-layers',
    children: [
      {
        name: 'Cronograma',
        url: '/Implementacion/Monitoreo/Cronograma',
        icon: 'icon-drop'
      },
      {
        name: 'Gestionar Secuencial',
        url: 'Implementacion/Monitoreo/GestSecuencial',
        icon: 'icon-drop'
      },
      {
        name: 'Gestionar Sectorista',
        url: '/Implementacion/Monitoreo/GestSectorista',
        icon: 'icon-drop'
      },
      {
        name: 'Informe',
        url: '/Implementacion/Monitoreo/Informe',
        icon: 'icon-drop'
      }
    ]
  },
  {
    title: true,
    name: 'Area de Usuarios'
  },
  {
    name: 'Gestionar',
    url: '/AreaUsuario/GestionUsuario',
    icon: 'icon-drop'
  },
  {
    name: 'Registrar',
    url: '/AreaUsuario/RegistroUsuario',
    icon: 'icon-drop'
  },
  {
    name: 'Mi Cuenta',
    url: '/AreaUsuario/MiCuenta',
    icon: 'icon-drop'
  },
  {
    name: 'Configuración',
    url: '/AreaUsuario/Config',
    icon: 'icon-drop'
  },
  {
    name: 'Cambiar Clave',
    url: '/AreaUsuario/CambioClave',
    icon: 'icon-drop'
  },
  {
    title: true,
    name: 'Reportes'
  },
  {
    name: 'Generar Reportes',
    url: '/Reportes/GenerarReporte',
    icon: 'icon-drop'
  },
/*
  {
    title: true,
    name: 'Theme'
  },
  {
    name: 'Colors',
    url: '/theme/colors',
    icon: 'icon-drop'
  },
  {
    name: 'Typography',
    url: '/theme/typography',
    icon: 'icon-pencil'
  },
  {
    title: true,
    name: 'Components'
  },
  {
    name: 'Base',
    url: '/base',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'Cards',
        url: '/base/cards',
        icon: 'icon-puzzle'
      },
      {
        name: 'Carousels',
        url: '/base/carousels',
        icon: 'icon-puzzle'
      },
      {
        name: 'Collapses',
        url: '/base/collapses',
        icon: 'icon-puzzle'
      },
      {
        name: 'Forms',
        url: '/base/forms',
        icon: 'icon-puzzle'
      },
      {
        name: 'Navbars',
        url: '/base/navbars',
        icon: 'icon-puzzle'

      },
      {
        name: 'Pagination',
        url: '/base/paginations',
        icon: 'icon-puzzle'
      },
      {
        name: 'Popovers',
        url: '/base/popovers',
        icon: 'icon-puzzle'
      },
      {
        name: 'Progress',
        url: '/base/progress',
        icon: 'icon-puzzle'
      },
      {
        name: 'Switches',
        url: '/base/switches',
        icon: 'icon-puzzle'
      },
      {
        name: 'Tables',
        url: '/base/tables',
        icon: 'icon-puzzle'
      },
      {
        name: 'Tabs',
        url: '/base/tabs',
        icon: 'icon-puzzle'
      },
      {
        name: 'Tooltips',
        url: '/base/tooltips',
        icon: 'icon-puzzle'
      }
    ]
  },
  {
    name: 'Buttons',
    url: '/buttons',
    icon: 'icon-cursor',
    children: [
      {
        name: 'Buttons',
        url: '/buttons/buttons',
        icon: 'icon-cursor'
      },
      {
        name: 'Dropdowns',
        url: '/buttons/dropdowns',
        icon: 'icon-cursor'
      },
      {
        name: 'Brand Buttons',
        url: '/buttons/brand-buttons',
        icon: 'icon-cursor'
      }
    ]
  },
  {
    name: 'Charts',
    url: '/charts',
    icon: 'icon-pie-chart'
  },
  {
    name: 'Icons',
    url: '/icons',
    icon: 'icon-star',
    children: [
      {
        name: 'CoreUI Icons',
        url: '/icons/coreui-icons',
        icon: 'icon-star',
        badge: {
          variant: 'success',
          text: 'NEW'
        }
      },
      {
        name: 'Flags',
        url: '/icons/flags',
        icon: 'icon-star'
      },
      {
        name: 'Font Awesome',
        url: '/icons/font-awesome',
        icon: 'icon-star',
        badge: {
          variant: 'secondary',
          text: '4.7'
        }
      },
      {
        name: 'Simple Line Icons',
        url: '/icons/simple-line-icons',
        icon: 'icon-star'
      }
    ]
  },
  {
    name: 'Notifications',
    url: '/notifications',
    icon: 'icon-bell',
    children: [
      {
        name: 'Alerts',
        url: '/notifications/alerts',
        icon: 'icon-bell'
      },
      {
        name: 'Badges',
        url: '/notifications/badges',
        icon: 'icon-bell'
      },
      {
        name: 'Modals',
        url: '/notifications/modals',
        icon: 'icon-bell'
      }
    ]
  },
  {
    name: 'Widgets',
    url: '/widgets',
    icon: 'icon-calculator',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    divider: true
  },
  {
    title: true,
    name: 'Extras',
  },
  {
    name: 'Pages',
    url: '/pages',
    icon: 'icon-star',
    children: [
      {
        name: 'Login',
        url: '/login',
        icon: 'icon-star'
      },
      {
        name: 'Register',
        url: '/register',
        icon: 'icon-star'
      },
      {
        name: 'Error 404',
        url: '/404',
        icon: 'icon-star'
      },
      {
        name: 'Error 500',
        url: '/500',
        icon: 'icon-star'
      }
    ]
  }

  */
];
